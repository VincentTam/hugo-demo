+++
title = "Home page"
+++

This bare markdown page has **bold** and *italics* formatting.

It links to <a href="./present">a hugo-reveal presentation</a> and an associated <a href="./present-info">bare markdown page</a>.

A sample leaf bundle: [`content1.md`](./leaf-bundle/content1/) and
[`content2.md`](./leaf-bundle/content2/)

[Another leaf bundle](./lb2/) for testing page resources
